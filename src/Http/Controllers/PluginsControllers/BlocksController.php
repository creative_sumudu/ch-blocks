<?php

namespace Creativehandles\ChBlocks\Http\Controllers\PluginsControllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Creativehandles\ChBlocks\Plugins\Blocks\Blocks as Blocks;
use Illuminate\View\View;

class BlocksController extends Controller
{
    protected $blocks;
    protected $views = 'Admin.Blocks.';

    public function __construct()
    {

        // Assign blocks plugin into variable
        $this->blocks = new Blocks();
    }

    /**
     * Render blocks index - folders page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view($this->views . "index")
            ->with("erasedFolders", $this->blocks->GetErasedOverFolders())
            ->with("folders", $this->blocks->GetOverFolders());
    }

    /**
     * Creates folder for blocks
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function CreateFolder(Request $request)
    {

        if ($request->has('folder')) {
            $folder = $this->blocks->createFolder($request->folder);

            return back()->with("created", $folder ? true : false);
        } else {
            return redirect(route('admin.blocks'))->with("noFolderQuery", true);
        }
    }


    /**
     * Creates subfodlerfolder for blocks
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function CreateSubFolder(Request $request, $id)
    {
        if ($request->has('folder')) {
            $folder = $this->blocks->createSubFolder($request->folder, $id);

            return back()->with("created", $folder ? true : false);
        } else {
            return redirect(route('admin.blocks'))->with("noFolderQuery", true);
        }
    }

    /**
     * Render grid of block inside folder
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function RenderFolder($id)
    {
        return view($this->views . 'folders')
            ->with('overFolder', $this->blocks->getOverfolder($id))
            ->with("erasedFolders", $this->blocks->GetErasedFolders())
            ->with('folders', $this->blocks->getOrderedFoldersInFolder($id, 'ASC'));
    }

    /**
     * Render grid of block inside folder
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function RenderSubFolder($folder, $subfolder)
    {

        return view($this->views . 'folder')
            ->with('overFolder', $this->blocks->getOverfolder($folder))
            ->with('folder', $this->blocks->getFolder($subfolder))
            ->with('blocks', $this->blocks->getOrderedBlocksInFolder($subfolder, 'ASC'));
    }


    /**
     * Render template for block creation
     *
     * @param $folder
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function RenderBlockForm($folder)
    {
        return view($this->views . 'blockForm')
            ->with('folder', $this->blocks->getFolder($folder));
    }

    /**
     * Save or Edit block in the DB
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function SaveBlock(Request $request)
    {
        $status = $this->blocks->save($request->all());
        if ($status) {
            $notif = isset($request->blockID) ? 'edited' : 'created';
            return redirect(route('admin.RenderSubFolder', [
                'subfolder' => $request->folderID,
                'folder' => $this->blocks->getSubfoldersOverFolder($request->folderID)->id
            ]))->with($notif, true);
        }
    }

    /**
     * Softly removes the block by given ID
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function RemoveBlock($id)
    {
        $status = $this->blocks->removeBlock($id);
        if ($status) {
            return back();
        }

        return back();
    }

    /**
     * Render template with block edit
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function RenderEditBlock($id)
    {
        $block = $this->blocks->getBlock($id);
        $folder = $this->blocks->getFolder($block->folder);
        $overFolder = $this->blocks->getOverfolder($folder->overfolder);

        return view($this->views . 'EditBlockForm')
            ->with('folder', $folder)
            ->with('block', $block)
            ->with('overFolder', $overFolder);
    }

    public function SaveBlocksPosition(Request $request)
    {
        if ($this->blocks->savePosition($request->array)) {
            return response()->json(['status' => true]);
        }

        return response()->json(['status' => false]);
    }

    public function RenameFolder(Request $request)
    {
        $status = $this->blocks->renameFolder($request->id, $request->folderName);

        return back()->with("success", $status);
    }


    /**
     * Softly removes folder and its content
     *
     * @param $id
     * @return $status
     */
    public function RemoveFolder($id)
    {
        $status = $this->blocks->removeFolder($id);
        return back()->with('status', $status);
    }

    /**
     * Softly removes folder and its content
     *
     * @param $id
     * @return $status
     */
    public function RemoveSubFolder($id)
    {
        $status = $this->blocks->RemoveSubFolder($id);
        return back()->with('status', $status);
    }


    public function RestoreFolder($id)
    {
        $status = $this->blocks->restoreFolder($id);
        return back()->with("status", $status);
    }

    public function RestoreOverFolder($id)
    {
        $status = $this->blocks->restoreOverFolder($id);
        return back()->with("status", $status);
    }

    /**
     * Makes a copy of block
     *
     * @param $id
     *
     * @return View;
     */
    public function DuplicateBlock($id)
    {
        $this->blocks->duplicateBlock($id);

        return back()->with("duplicated", true);
    }
}
